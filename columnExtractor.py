#we used this to extract the three columns we needed from the dataset. 
#Our original dataset has 22 columns but for our research we only need 3 of them which are the uid, campaign and timestamp
#Please remeber to change the path when using this code. 
fileNumber = 1

uid = []
campaign = []
timestamp = []

extracted = [uid, campaign, timestamp]

def extractColumn():
	inputFile = open("/home/agetache/Attribution/clicktimefile/file" + str(fileNumber) + ".csv", "r+")
	#inputFile = open("/home/agetache/Attribution/neededFiles/conversion.csv", "r+")
	lines = inputFile.read().splitlines()

	inputFile.seek(0) # moves to beginning of file cause the current position is end of file
	inputFile.truncate() # resizes / clears the file

	# print(lines)
	
	for line in lines:
		lineArray = line.split(',')
		uid.append(lineArray[0])
		campaign.append(lineArray[1])
		timestamp.append(lineArray[9])
		# thirdColumn = line.split(' ')[2].strip()
		# removedLine = line.replace(thirdColumn, '')
		# inputFile.write("%s\n" %removedLine.strip())

	for index in range(len(extracted[0])):
		for item in extracted:
			inputFile.write("%s " %item[index])
			# print(extracted[i][index], end=" ") 
		# print()
		inputFile.write("\n")

	# print(uid)
	# print(campaign)
	# print(timestamp)
	# print(extracted)
	inputFile.close()


if __name__ == '__main__':
	numberOfFiles = input("Enter number of files: ")
	while (fileNumber <= numberOfFiles):
	        extractColumn()
		fileNumber += 1
	print("Done!")

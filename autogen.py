#Used to generate files automatically. For us, we have generated 134 files for 'Attribution = 1' , 'Conversion = 1' and 'click = 1'. 
#Range of time used in this case is "20,000s". 
#please remember to change the path when using this.

inputFile = open("/home/agetache/DYNAMICDATASETS/Attribution/neededFiles/attribution.csv","r")
lines = inputFile.read().splitlines() 

fileNumber = 0
timeFactorMin = 0
timeFactorMax = timeFactorMin + 20000

def doTask():
	newFile = open("/home/agetache/DYNAMICDATASETS/Attribution/generatedfiles/attrtimefile/File" + str(fileNumber) + ".txt", "w")
	count = 0
	for line in lines:
		timestampColumn = line.split(',')[9].strip()
		if (timestampColumn == 'timestamp'):
			continue
	#	if (eval(timestampColumn) <= timeFactorMin):
	#		continue
		if (eval(timestampColumn) >= timeFactorMax):
			break
		else:
			results = line.split(',')
			newFile.write("%s %s\n" %(results[0].strip(),results[1].strip()))
			count += 1
	print(count)

	newFile.close()


if __name__  == "__main__":
	totalNumberOfFiles = input("Enter the total number of files: ")

	while(fileNumber <= totalNumberOfFiles):
		doTask()
		fileNumber += 1
		timeFactorMin = timeFactorMax
		timeFactorMax = timeFactorMin + 20000

	inputFile.close()

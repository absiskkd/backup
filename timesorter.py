#used to sort our files using the timestamp values. 
#Please change the path when using this code.
inputFile = open("/home/agetache/Attribution/neededFiles/attribution.csv","r")
lines = inputFile.read().splitlines()

def takeThirdColumn(line):                      # taking the third column of every row passed for sorting
        return line.split(',')[9].strip()


#print(lines)
#print("\n --- SORTED --- \n")
sortedRows = sorted(lines,key=takeThirdColumn) # this built in sort function sorts the list using the third column since we specified it as a key
#print(sortedRows)

inputFile.close()

sortedInputFile = open("/home/agetache/Attribution/sortedFile/attributionsorted.txt", "w")
for sortedRow in sortedRows:
        sortedInputFile.write("%s\n" %sortedRow)

#newFile = open("outputFile.txt", "w")
#newFile.write("Numbers from column 3 below 300 are:\n")

#for line in sortedRows:
#       thirdColumn = line.split(' ')[2].strip()
#
#       if (eval(thirdColumn) < 300):
#               newFile.write("%s\n" %line.strip())

#newFile.close()

sortedInputFile.close()


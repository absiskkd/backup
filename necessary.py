#Used to parse out the files we need. In this case we used it to get the rows whose attribution, conversion and click column values are one. We do each in a different output file. 
#Please remember to change path when using this code. 

import csv 

with open('criteo_attribution_dataset.tsv') as tsvfile:
    reader = csv.DictReader(tsvfile, dialect = 'excel-tab')
    attribution_1 = []
    for i, row in enumerate(reader):
        if(row['click'] == '1'):
            attribution_1.append(row)
    print(len(attribution_1))
    output = open('click.csv' , 'w')
    writer = csv.DictWriter(output, fieldnames=attribution_1[1].keys())
    writer.writeheader()
    for row in attribution_1:
        writer.writerow(row)
    # writer.writerows(attribution_1)

tsvfile.close()
output.close()
